const Char = (props) => {
    const style = {
        display: 'inline-block',
        padding: '16px',
        'text-aline':'center',
        margin: '16px',
        border: '1px solid black'
    }
    
    return(<p style={style} onClick={(event)=>props.click(props.uid)}>{props.char}</p>)
}

export default Char;