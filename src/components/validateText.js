

const ValidateText = (props) => {
    
    const renderError = () => {
        return props.length < 5? <p>Text is too short</p> : <p>Text is long enough </p>;
    }

    return (
        <div>
            {renderError()}
        </div>);
}

export default ValidateText;