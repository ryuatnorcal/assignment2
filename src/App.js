import React, {useState, useEffect} from 'react';
import './App.css';
import Validate from './components/validateText';
import Char from './components/char';

function App() {
  const [leng, setLeng] = useState(0);
  const [text, setText] = useState('');
  const [results,setResults] = useState([]);
  useEffect(()=>{

  },[leng,results,text]);

  const changeTextHandler = event => {
    const newVal = event.target.value;
    event?.target?.value? setLeng(newVal.length) : setLeng(0);
    event?.target?.value? setText(newVal) : setText('');
    mapCharCompoents();
  } 

  const deleteCharHandler = (id) => {
    const r = [...results];
    r.splice(id,1);
    setResults(r);
    let char = text.split('');
    char.splice(id,1);
    const str = char.join('');
    setText(str);
    setLeng(str.length);
    
  }

  const mapCharCompoents = () => {
    if(!text || typeof text !== 'string') return ''; 
    const chars = text.split('');
    const res = chars.map( (c,index) => {
      return <Char key={c+'-'+index} uid={index} char={c} click={deleteCharHandler}/>
    }); 
    setResults(res);
    return res;
  }

  return (
    <div className="App">
      <textarea 
        onChange={changeTextHandler}
        value={text}
      ></textarea>
      <p>Length of the input: {leng}</p>
      <Validate length={leng} />
      {results}
    </div>
  );
}

export default App;
